package jconfig.props;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.io.IOException;
import java.util.Set;

class JConfigTest {
    private JConfig config;

    @BeforeEach
    void loadConfiguration() throws IOException {
        config = new JConfig();
        config.loadConfiguration("src/test/test.properties");
    }

    @Test
    void getStringSuccess() {
        String value = config.getString("string-property");
        assert (value.equals("test property"));
    }

    @Test
    void getStringsSuccess() {
        Set<String> propSet = config.getStrings("string-properties");
        assert (propSet.size() == 4);
    }

    @Test
    void getIntegerSuccess() {
        Integer i = config.getInteger("int-property");
        assert (i == 1);
    }

    @Test
    void getIntegerFail() {
        Assertions.assertThrows(Exception.class, new Executable() {
            @Override
            public void execute() throws Throwable {
                config.getInteger("string-property");
            }
        });
    }
}
package jconfig.props;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

public class JConfig {

    private Properties properties = new Properties();
    private static String osName = System.getProperty("os.name").toUpperCase();

    public void loadConfiguration(String filename) throws IOException {
        try (FileInputStream fis = new FileInputStream(filename)) {
            properties.load(fis);
        }
    }

    public String getString(String key) {
        String value = System.getenv(key);
        if (value == null || value.isEmpty()) {
            value = properties.getProperty(key);
        }
        return value;
    }

    public Integer getInteger(String key) {
        String value = getString(key);
        if (value == null || value.isEmpty()) {
            return null;
        }
        return Integer.valueOf(value);
    }

    public Long getLong(String key) {
        String value = getString(key);
        if (value == null || value.isEmpty()) {
            return null;
        }
        return Long.valueOf(value);
    }

    public Set<String> getStrings(String key) {
        Set<String> propSet = new HashSet<>();
        String value = System.getenv(key);

        if (value != null && !value.isEmpty()) {
            String envDelimiter = ":";
            if (isWindows()) {
                envDelimiter = ";";
            }
            String[] values = value.split(envDelimiter);
            propSet.addAll(Arrays.asList(values));

        } else {
            Set<String> propNames = properties.stringPropertyNames();
            for (String propName : propNames) {
                if (propName.startsWith(key)) {
                    propSet.add(properties.getProperty(propName));
                }
            }
        }
        return propSet;
    }

    private boolean isWindows() {
        return osName.startsWith("WINDOWS");
    }

}
